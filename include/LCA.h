#ifndef __LCA_H_
#define __LCA_H_
#include "RMQ.h"
#include <vector>
#include "NormalizedBlock.h"
class LCA
{
	private:
		int *m_pA;
		int m_size;
		int m_log_size;
		int m_slots;
		int m_slot_size;
		int m_block_type;
		int *m_pSlotToBlockType;
		int *m_pADash;
		//int *m_pADashRMQ;
		std::vector<NormalizedBlock> m_vNormalized;
		RangeMinimumQuery* m_pADashRMQ;
		int GetBlockType(int start, int end);
	public:
		LCA (std::vector<int> A);
		~LCA ();
		void printValues ();
		int GetAncestor(int a, int b);
		int GetRangeMinimum (int start, int end);
};

#endif

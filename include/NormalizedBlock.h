#ifndef __NB_H_
#define __NB_H_

class NormalizedBlock
{
	public:
		NormalizedBlock(int size);
		virtual ~NormalizedBlock();
		void SetType (int type) {m_type = type;};
		int GetMin (int start, int end);
		void Compute();
		void PrintLookup();
	private:
		int m_size;
		int m_type;
		int **lookup;
};

#endif

#ifndef _RMQ_H_
#define _RMQ_H_
#include <vector>
class RangeMinimumQuery
{
	private:
		int m_size;
		int m_ranges;
		int *pA;
		int *pLookUp;
		void generateLookUp();
		int getMinIndex (int, int);
		int index (int x, int y) {
			return m_size*y + x;
		}

	public:
		int getSize () {return m_size;}
		RangeMinimumQuery (int );
		RangeMinimumQuery (int*, int);
		RangeMinimumQuery (std::vector<int> A);
		~RangeMinimumQuery ();
		int RangeQuery (int start, int end);
		void printValues ();
		void printLookUp();
};
#endif

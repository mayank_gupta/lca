#ifndef TREE_H
#define TREE_H
#include <vector>
#include <cstddef>
#include <map>
struct Node {
    int id;
    struct Node* left, *right;
    Node (int i)
    {
        id = i;
        left=right=NULL;
    }
};
typedef struct Node Node;
class Tree
{
    public:
        Tree();
        virtual ~Tree();
        void AddNode (int id);
        Node *GetNode (int id);
        Node *GetNodeAtEulerIndex (int id);
        int GetEulerIndex (Node *r);
        std::vector<int> GetEulerTour ();
        void ComputeEulerTour();
    protected:
    private:
        Node* AddNodeRecursive (Node *r, int id);
        Node* GetNodeRecursive (Node *r, int id);
        void EulerTourRecursive (Node *r, int depth);
        Node *m_pRoot;
        int m_size;
        std::vector<int> m_EulerTourDepth;
        std::vector<Node*> m_EulerTourMap;
        std::map<Node*, int> m_NodeEulerIndex;
};

#endif // TREE_H

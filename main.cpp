#include <iostream>
#include "Tree.h"
#include "RMQ.h"
#include "LCA.h"
using namespace std;

int main ()
{
    Tree *pTree = new Tree();
    pTree->AddNode(5);
    pTree->AddNode(3);
    pTree->AddNode(4);
    pTree->AddNode(2);
    pTree->AddNode(7);
    pTree->AddNode(6);
    pTree->ComputeEulerTour();
    vector<int> et = pTree->GetEulerTour();
    int index = 0;
    for (vector<int>::iterator it = et.begin(); it!= et.end(); it++)
    {
        cout << *it << ":" << pTree->GetNodeAtEulerIndex(index)->id << "\t";
        index++;
    }
    cout << endl;
    Node *n = pTree->GetNode(6);
    Node *m = pTree->GetNode(2);
    cout << "Index of 6:" << pTree->GetEulerIndex(n) << endl;;

    //RangeMinimumQuery* rmq = new RangeMinimumQuery(et);
    Node *a = pTree->GetNode(3);
    Node *b = pTree->GetNode(6);
    int euler_index_n = pTree->GetEulerIndex(a);
    int euler_index_m = pTree->GetEulerIndex(b);
    //int ancestor_index = rmq->RangeQuery(euler_index_n, euler_index_m);
    //cout << "LCA:" << pTree->GetNodeAtEulerIndex(ancestor_index)->id << " index:" << ancestor_index << endl;

    LCA *pLCA = new LCA(et);
    //Node *a = pTree->GetNode(3);
    //Node *b = pTree->GetNode(4);
    //int euler_index_n = pTree->GetEulerIndex(a);
    //int euler_index_m = pTree->GetEulerIndex(b);
    //pLCA->GetRangeMinimum(euler_index_n, euler_index_m);
    int ancestor_index2 = pLCA->GetAncestor(euler_index_n, euler_index_m);
    cout << "LCA:" << pTree->GetNodeAtEulerIndex(ancestor_index2)->id << " index:" << ancestor_index2 << endl;
    return -1;
}

#include <iostream>
#include "LCA.h"
#include "RandomNumberGenerator.h"
#include <iostream>
#include <cmath>
using namespace std;
LCA::LCA(vector<int> A)
{
    m_size = A.size();
	m_log_size = (int)ceil(log(m_size)) + 1;
	m_slot_size = (int)ceil((float)m_log_size/(float)2);
	m_slots = (int)ceil((float)m_size/(float)m_slot_size);
    m_block_type = (int)ceil(sqrt(m_size));
    cout << "size:" << m_size << " log_size:" << m_log_size << " Slot_size:" << m_slot_size;
    cout << " m_slots:" << m_slots << " m_blockTypes:" << m_block_type << endl;

    m_pA = new int[m_size];
    int index = 0;
    for (vector<int>::iterator it = A.begin(); it!= A.end(); it++)
    {
        m_pA[index++] = (*it);
        cout << m_pA[index-1] << "\t";
    }
    cout << endl;

    for (int i=0; i < m_block_type; i++)
    {
        NormalizedBlock n(m_slot_size);
        n.SetType(i);
        n.Compute();
        m_vNormalized.push_back(n);
    }
    cout << "Computed normalized blocks" << endl;
    m_pSlotToBlockType = new int[m_slots];
    m_pADash = new int[m_slots];
    for (int i=0; i < m_slots; i++)
    {
        int start = i*m_slot_size;
        int end = (i+1)*(m_slot_size);
        if (end > m_size) end = m_size;
        cout << "Start:"<< start << " End:" << end << endl;
        m_pSlotToBlockType[i] = GetBlockType(start, end);
        int local_min_index = m_vNormalized[m_pSlotToBlockType[i]].GetMin(0, m_slot_size-1);
        m_pADash[i] = m_pA[start+local_min_index];
    }
    m_pADashRMQ = new RangeMinimumQuery(m_pADash, m_slots);
	/*

	RandomNumberGenerator r;
	m_pA = new int[m_size];
	for (int i=0; i < m_size; i++)
	{
		m_pA[i] = r.getRand(2);
	}
	std::cout<<"Slot size: " << m_slot_size<<std::endl;
	std::cout<<"#Slots: " << m_slots << std::endl;
	for (int i=0; i < m_slots; i++)
	{
		int rmqSize = m_slot_size;
		int start_index = i*m_slot_size;
		if (m_size - start_index < m_slot_size)
			rmqSize = m_size - start_index;
		//std::cout<< "Start: " << i*m_slot_size << " End: " << i*m_slot_size + rmqSize << std::endl;
		v_rmq.push_back(RangeMinimumQuery(m_pA + i*m_slot_size, rmqSize));
	}
	int * a = new int[m_slots];
	for (int i=0; i<m_slots; i++)
	{
		int s = v_rmq[i].getSize();
		int min = v_rmq[i].RangeQuery(0, s-1);
		a[i] = m_pA[i*m_slot_size + min];
		cout << a[i] << "\t";
	}
	cout << endl;
	m_pADash = new RangeMinimumQuery(a, m_slots);
	*/
}

LCA::~LCA()
{
}

int LCA::GetBlockType(int start, int end)
{
    int counter = 0;
    for (int i=start; i < end; i++)
    {
        counter = counter << 1;
        if (i-1 >= 0 && m_pA[i] > m_pA[i-1])
        {
            counter |= 1;
        }
        else
        {
            counter |= 0;
        }

    }
    cout << "Counter:" << counter << endl;
    return counter;
}
void LCA::printValues()
{
/*
	using namespace std;
	for (int i=0; i < m_size; i++)
	{
		cout << m_pA[i] << "\t";
	}
	cout<<endl;
	for (vector<RangeMinimumQuery>::iterator it=v_rmq.begin(); it!=v_rmq.end(); it++)
	{
		it->printValues();
		//it->printLookUp();
	}
	m_pADash->printValues();
	m_pADash->printLookUp();
	*/
}

int LCA::GetAncestor(int a, int b)
{
	int start_block = a/m_slot_size;
	int end_block = b/m_slot_size;

	if (start_block == end_block)
	{
        cout << "Same block" << endl;
        int blocktype = m_pSlotToBlockType[start_block];
        int local_min = m_vNormalized[blocktype].GetMin(0, m_slot_size);
        return start_block*m_slot_size + local_min;
		//1return v_rmq[start_block].RangeQuery(a%m_slot_size, b%m_slot_size);
	}
	else
	{
        cout << "different block" << endl;
		int left= start_block*m_slot_size + m_vNormalized[m_pSlotToBlockType[start_block]].GetMin(a%m_slot_size, m_slot_size-1);
		int right = end_block*m_slot_size;
		int right_local_min = m_vNormalized[m_pSlotToBlockType[end_block]].GetMin(0, b%m_slot_size);
		cout << "Right local min:" << right_local_min << " Block Type:" <<m_pSlotToBlockType[end_block] << endl;
		right+= right_local_min;
		int mid = m_pADashRMQ->RangeQuery(start_block+1, end_block-1);
		mid = mid*m_slot_size + m_vNormalized[m_pSlotToBlockType[mid]].GetMin(0, m_slot_size);
        cout << "left:" << left << " Mid:" << mid << " Right:" << right << endl;
		if (m_pA[left] < m_pA[right] && m_pA[left] < m_pA[mid])
            return left;
        else if (m_pA[right] < m_pA[left] && m_pA[right] < m_pA[mid])
            return right;
        else
            return mid;
	}
	return -1;
}

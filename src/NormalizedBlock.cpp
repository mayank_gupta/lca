#include "NormalizedBlock.h"
#include <iostream>
using namespace std;
NormalizedBlock::~NormalizedBlock()
{
    return;
	for (int i=0; i < m_size; i++)
	{
		delete lookup[i];
	}
	delete lookup;
	lookup = NULL;
}

NormalizedBlock::NormalizedBlock (int size)
{
	m_size = size;
	lookup = new int*[m_size];
	for (int i=0; i < m_size; i++)
	{
		lookup[i] = new int[m_size];
	}
}
int NormalizedBlock::GetMin(int start, int end)
{
	return lookup[start][end];
}

void NormalizedBlock::Compute()
{
	int counter, index, min;
	for (int i=0; i <m_size; i++)
	{
		cout << ((m_type>>i)&1);
	}
	cout << endl;
	for (int j=0; j<m_size; j++)
	{
		min = 1000;
		index = -1;
		counter = 0;
		for (int i=j; i < m_size ; i++)
		{
			if (m_type>>i & 1)
			{
				//cout << "+1" << endl;
				counter++;
			}
			else
			{
				//cout << "-1" << endl;
				counter--;
			}
			if (min > counter)
			{
				min = counter;
				index = i;
			}
			lookup[j][i] = index;
		}
	}
}

void NormalizedBlock::PrintLookup()
{
	for (int i=0; i<m_size; i++)
	{
		for (int j=i; j<m_size; j++)
		{
			cout << lookup[i][j] << "\t";
		}
		cout << endl;
	}
}
/*
using namespace std;
int main ()
{
	NormalizedBlock n(5);
	n.SetType(7);
	n.Compute();
	n.PrintLookup();
	int start = 0, endd = 4;
	cout << "Start:" << start << " End:" << endd ;
	cout << " Min:" << n.GetMin(start, endd) << endl;
	return -1;
}*/

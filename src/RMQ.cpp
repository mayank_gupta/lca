#include "RMQ.h"
#include "RandomNumberGenerator.h"
#include <iostream>
#include <cmath>
using namespace std;
RangeMinimumQuery::RangeMinimumQuery (int size)
{
	RandomNumberGenerator r;
	m_size = size;
	m_ranges = (int)ceil(log(m_size))+1;
	pA = new int[m_size];
	pLookUp = new int[m_size*m_ranges];
	for (int i=0; i < m_size*m_ranges; i++)
	{
		pLookUp[i] = -1;
	}
	for (int i=0; i<m_size; i++)
	{
		pA[i] = r.getRand(100);
		pLookUp[index(i,0)] = i;
	}
	generateLookUp();
}

RangeMinimumQuery::RangeMinimumQuery(int *a, int size)
{
	m_size = size;
	m_ranges = (int)ceil(log(m_size))+1;
	pA = a;
	pLookUp = new int[m_size*m_ranges];
	for (int i=0; i < m_size*m_ranges; i++)
	{
		pLookUp[i] = -1;
	}
	for (int i=0; i<m_size; i++)
	{
		pLookUp[index(i,0)] = i;
	}
	generateLookUp();
}
RangeMinimumQuery::RangeMinimumQuery(vector<int> A)
{
    m_size = A.size();
	m_ranges = (int)ceil(log(m_size))+1;
	pA = new int[m_size];
	for (int i=0; i < m_size; i++)
	{
        pA[i] = A[i];
	}
	pLookUp = new int[m_size*m_ranges];
	for (int i=0; i < m_size*m_ranges; i++)
	{
		pLookUp[i] = -1;
	}
	for (int i=0; i<m_size; i++)
	{
		pLookUp[index(i,0)] = i;
	}
	generateLookUp();
}
RangeMinimumQuery::~RangeMinimumQuery()
{
	delete pLookUp;
	delete pA;
}

void RangeMinimumQuery::printValues ()
{
	using namespace std;
	for (int i=0; i < m_size; i++)
	{
		cout << pA[i] << "\t";
	}
	cout << endl;
}

void RangeMinimumQuery::printLookUp()
{
	using namespace std;
	for (int y=0; y < m_ranges; y++)
	{
		for (int x=0; x < m_size; x++)
		{
			cout << pLookUp[index(x,y)] << "\t";
		}
		cout << endl;
	}
}
int RangeMinimumQuery::getMinIndex (int i1, int i2)
{
	if (pA[i1] < pA[i2])
		return i1;
	return i2;
}
void RangeMinimumQuery::generateLookUp()
{
	using namespace std;
	cout << "Start. " << m_size << "range:" << m_ranges << endl;
	for (int y=1; y < m_ranges; y++)
	{
		for (int x=0; x <m_size; x++)
		{
			if (x + y >= m_size) break;
			pLookUp[index(x,y)] = getMinIndex(pLookUp[index(x,y-1)], pLookUp[index(x+1, y-1)]);
		}
	}
}

int RangeMinimumQuery::RangeQuery(int start, int end)
{
	if (start == end || start > end) return -1;
	int width = (int)ceil(log(end - start));
	std::cout << "Width:" << width << endl;
	int lMin = pLookUp[index(start, width)];
	int rMin = pLookUp[index(end-width, width)];
	return getMinIndex(lMin, rMin);
}

/*
int main ()
{
	RangeMinimumQuery* rmq = new RangeMinimumQuery(10);
	rmq->printValues();
	rmq->printLookUp();
	cout << "Meh" << endl;
	std::cout << "Min:" << rmq->RangeQuery(0,9) << std::endl;
	return -1;
}
*/

#include "Tree.h"
using namespace std;
Tree::Tree()
{
    m_pRoot = NULL;
    //ctor
}

Tree::~Tree()
{
    //dtor
}

Node* Tree::AddNodeRecursive(Node* r, int id)
{
    if (r==NULL)
    {
        r = new Node (id);
    }
    else if (r->id > id)
    {
        r->left = AddNodeRecursive(r->left, id);
    }
    else
    {
        r->right = AddNodeRecursive(r->right, id);
    }
    return r;
}

void Tree::AddNode (int id)
{
    m_pRoot = AddNodeRecursive(m_pRoot, id);
    m_size++;
}

Node* Tree::GetNodeRecursive(Node *r, int id)
{
    if (r==NULL)
        return NULL;
    else if (r->id == id)
        return r;
    else if (r->id > id)
        return GetNodeRecursive(r->left, id);
    else
        return GetNodeRecursive(r->right, id);
}

Node * Tree::GetNode (int id)
{
    return GetNodeRecursive(m_pRoot, id);
}

Node *Tree::GetNodeAtEulerIndex (int id)
{
    return m_EulerTourMap[id];
}

void Tree::EulerTourRecursive(Node *r, int depth)
{
    if (r==NULL)
        return;
    m_EulerTourMap.push_back(r);
    m_EulerTourDepth.push_back(depth);
    m_NodeEulerIndex[r] = m_EulerTourDepth.size() - 1;
    if (r->left !=NULL)
    {
        EulerTourRecursive(r->left, depth+1);
        m_EulerTourMap.push_back(r);
        m_EulerTourDepth.push_back(depth);
    }
    if (r->right != NULL)
    {
        EulerTourRecursive(r->right, depth+1);
        m_EulerTourMap.push_back(r);
        m_EulerTourDepth.push_back(depth);
    }
}

vector<int> Tree::GetEulerTour ()
{
    return m_EulerTourDepth;
}

void Tree::ComputeEulerTour()
{
    EulerTourRecursive(m_pRoot, 0);
}

int Tree::GetEulerIndex(Node *n)
{
    return m_NodeEulerIndex[n];
}
